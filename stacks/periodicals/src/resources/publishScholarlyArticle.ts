import {
  PostPublishScholarlyArticleRequest, PostPublishScholarlyArticleResponse,
} from '../../../../lib/interfaces/endpoints/periodical';
import { Request } from '../../../../lib/lambda/faas';
import { DbContext } from '../../../../lib/lambda/middleware/withDatabase';
import { SessionContext } from '../../../../lib/lambda/middleware/withSession';
import { isArticleAuthor } from '../../../../lib/utils/periodicals';
import {
  scholarlyArticleDescriptionValidators,
  scholarlyArticleNameValidators,
} from '../../../../lib/validation/scholarlyArticle';
import { isValid } from '../../../../lib/validation/validators';
import { publishScholarlyArticle as service } from '../services/publishScholarlyArticle';
import { fetchScholarlyArticle } from '../services/scholarlyArticle';

export async function publishScholarlyArticle(
  context: Request<PostPublishScholarlyArticleRequest> & DbContext & SessionContext,
): Promise<PostPublishScholarlyArticleResponse> {
  if (!context.body) {
    throw new Error('Please specify an article to publish.');
  }

  if (
    !context.body ||
    !context.body.targetCollection ||
    !context.body.targetCollection.identifier
  ) {
    throw new Error('Please specify an article to publish.');
  }
  const articleIdentifier = context.body.targetCollection.identifier;

  if (context.session instanceof Error) {
    throw new Error('You do not appear to be logged in.');
  }
  const session = context.session;

  if (!session.account) {
    throw new Error('You can only publish an article if you have created an account.');
  }

  const storedArticle = await fetchScholarlyArticle(context.database, articleIdentifier, context.session);

  if (storedArticle === null) {
    throw new Error(`Could not find an article with ID ${articleIdentifier}.`);
  }

  const articleAuthor = (storedArticle.author) ? storedArticle.author : undefined;
  if (
      storedArticle.creatorSessionId !== session.identifier
      && (!articleAuthor || !isArticleAuthor({ author: articleAuthor }, context.session))
   ) {
    throw new Error('You can only publish your own articles.');
  }
  if (!storedArticle.description || !isValid(storedArticle.description, scholarlyArticleDescriptionValidators)) {
    throw new Error('The article has to have a valid abstract before it can be published.');
  }
  if (!storedArticle.name || !isValid(storedArticle.name, scholarlyArticleNameValidators)) {
    throw new Error('The article has to have a valid name before it can be published.');
  }

  try {
    const result = await service(context.database, articleIdentifier, session.account);

    return {
      result: {
        author: { identifier: session.account.identifier },
        datePublished: result.datePublished,
        identifier: result.identifier,
      },
      targetCollection: {
        identifier: articleIdentifier,
      },
    };
  } catch (e) {
    // tslint:disable-next-line:no-console
    console.log('Database error:', e);

    throw new Error('There was a problem publishing the article, please try again.');
  }
}
