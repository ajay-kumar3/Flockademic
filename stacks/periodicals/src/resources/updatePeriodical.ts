import {
  PostUpdatePeriodicalRequest,
  PostUpdatePeriodicalResponse,
} from '../../../../lib/interfaces/endpoints/periodical';
import { Periodical } from '../../../../lib/interfaces/Periodical';
import { Request } from '../../../../lib/lambda/faas';
import { DbContext } from '../../../../lib/lambda/middleware/withDatabase';
import { SessionContext } from '../../../../lib/lambda/middleware/withSession';
import {
  periodicalDescriptionValidators,
  periodicalHeadlineValidators,
  periodicalNameValidators,
} from '../../../../lib/validation/periodical';
import { isValid } from '../../../../lib/validation/validators';
import { updatePeriodical as service } from '../services/updatePeriodical';
import { canManagePeriodical } from '../utils/canManagePeriodical';

export async function updatePeriodical(
  context: Request<PostUpdatePeriodicalRequest> & DbContext & SessionContext,
): Promise<PostUpdatePeriodicalResponse> {
  if (!context.body || !context.body.object) {
    throw new Error('Please specify properties to update.');
  }
  const proposedPeriodical: Partial<Periodical> = context.body.object;

  if (context.session instanceof Error) {
    throw context.session;
  }

  if (!context.body.targetCollection || !context.body.targetCollection.identifier) {
    throw(new Error('No journal to update specified'));
  }

  const allowedToEdit =
    await canManagePeriodical(context.database, context.body.targetCollection.identifier, context.session);
  if (!allowedToEdit) {
    throw new Error('You can only update journals you manage.');
  }

  const toUpdate: {
    description?: string;
    headline?: string;
    name?: string;
  } = {};
  if (isValid(proposedPeriodical.description, periodicalDescriptionValidators)) {
    toUpdate.description = proposedPeriodical.description;
  }
  if (isValid(proposedPeriodical.headline, periodicalHeadlineValidators)) {
    toUpdate.headline = proposedPeriodical.headline;
  }
  if (proposedPeriodical.name && isValid<string>(proposedPeriodical.name, periodicalNameValidators)) {
    toUpdate.name = proposedPeriodical.name;
  }

  try {
    const result = await service(
      context.database,
      context.body.targetCollection.identifier,
      toUpdate,
    );

    const datePublished = (result.date_published) ? result.date_published.toISOString() : undefined;

    return {
      result: {
        datePublished,
        description: result.description,
        headline: result.headline,
        name: result.name,
      },
      targetCollection: {
        identifier: context.body.targetCollection.identifier,
      },
    };
  } catch (e) {
    throw new Error('There was a problem modifying the journal, please try again.');
  }
}
