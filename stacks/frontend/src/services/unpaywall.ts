import { GetOaLocationsResponse, isValidOaLocationsResponse } from '../../../../lib/interfaces/endpoints/unpaywall';

export async function getOpenAccessLocation(doi: string): Promise<string | null> {
  const response = await fetch(`https://api.unpaywall.org/v2/${doi}?email=api.unpaywall.org@flockademic.com`);

  const data: GetOaLocationsResponse = await response.json();

  if (!isValidOaLocationsResponse(data)) {
    return null;
  }

  if (data.best_oa_location === null) {
    return null;
  }

  return data.best_oa_location.url_for_pdf || data.best_oa_location.url;
}
