jest.mock('../../src/services/fetchResource');
jest.mock('../../src/services/unpaywall', () => ({
  getOpenAccessLocation: jest.fn().mockReturnValue(Promise.resolve('arbitrary-location')),
}));

import {
  GetOrcidProfileSearchResponse,
  GetOrcidResponse,
  GetOrcidWorkResponse,
} from '../../../../lib/interfaces/endpoints/orcid';
import { getProfile, getWork, searchProfiles } from '../../src/services/orcid';

describe('getProfile', () => {
  const mockResponse: GetOrcidResponse = {
    'activities-summary': {
      educations: { 'education-summary': [] },
      employments: { 'employment-summary': [] },
      works: { group: [] },
    },
    'person': {
      'biography': null,
      'keywords': { keyword: [] },
      'name': {
        'family-name': null,
        'given-names': { value: 'Arbitrary name' },
      },
      'researcher-urls': { 'researcher-url': [] },
    },
  };

  it('should return a Person for a minimal API response', () => {
    const mockMinimalResponse: GetOrcidResponse = {
      ...mockResponse,
      person: {
        ...mockResponse.person,
        name: {
          ...mockResponse.person.name,
          'given-names': { value: 'Some name' },
        },
      },
    };

    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce({ json: () => (mockMinimalResponse) });

    return expect(getProfile('some-orcid')).resolves.toEqual([
      {
        name: 'Some name',
        sameAs: [ 'https://orcid.org/some-orcid' ],
      },
      [],
    ]);
  });

  it('should return a Person and a list of Articles for a maximally detailed API response', () => {
    const mockMaximalResponse: GetOrcidResponse = {
      'activities-summary': {
        educations: { 'education-summary': [
          {
            'end-date': { year:  { value: '1337' }, month: { value: '07' }, day: { value: '02' } },
            'organization': { name: 'Some education organisation' },
            'role-title': 'Some education role',
            'start-date': { year: { value: '1337' }, month: { value: '04' }, day: { value: '02' } },
          },
          {
            'end-date': null,
            'organization': { name: 'Some other education organisation' },
            'role-title': 'Some other education role',
            'start-date': { year: { value: '1337' }, month: { value: '04' }, day: { value: '02' } },
          },
        ] },
        employments: { 'employment-summary': [
          {
            'end-date': { year:  { value: '1337' }, month: { value: '07' }, day: { value: '02' } },
            'organization': { name: 'Some employment organisation' },
            'role-title': 'Some employment role',
            'start-date': { year:  { value: '1337' }, month: { value: '04' }, day: { value: '02' } },
          },
          {
            'end-date': null,
            'organization': { name: 'Some other employment organisation' },
            'role-title': 'Some other employment role',
            'start-date': { year:  { value: '1337' }, month: { value: '04' }, day: { value: '02' } },
          },
        ] },
        works: { group: [
          {
            'work-summary': [
              {
                'put-code': 1337,
                'title': {
                  title: { value: 'Some article title' },
                },
              },
            ],
          },
          {
            'work-summary': [
              {
                'put-code': 42,
                'title': {
                  title: { value: 'Some other article title' },
                },
              },
            ],
          },
        ] },
      },
      'person': {
        'biography': { content: 'Some biography' },
        'keywords': { keyword: [] },
        'name': {
          'family-name': { value: 'Some family name' },
          'given-names': { value: 'Some first name' },
        },
        'researcher-urls': { 'researcher-url': [
          { 'url-name': 'Some website name', 'url': { value: 'some-url' } },
        ] },
      },
    };

    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce({ json: () => (mockMaximalResponse) });

    return expect(getProfile('some-orcid')).resolves.toEqual([
      {
        affliation: [
          {
            affliation: { name: 'Some education organisation' },
            endDate: '1337-07-02T00:00:00.000Z',
            roleName: 'Some education role',
            startDate: '1337-04-02T00:00:00.000Z',
          },
          {
            affliation: { name: 'Some other education organisation' },
            endDate: undefined,
            roleName: 'Some other education role',
            startDate: '1337-04-02T00:00:00.000Z',
          },
          {
            affliation: { name: 'Some employment organisation' },
            endDate: '1337-07-02T00:00:00.000Z',
            roleName: 'Some employment role',
            startDate: '1337-04-02T00:00:00.000Z',
          },
          {
            affliation: { name: 'Some other employment organisation' },
            endDate: undefined,
            roleName: 'Some other employment role',
            startDate: '1337-04-02T00:00:00.000Z',
          },
        ],
        description: 'Some biography',
        name: 'Some first name Some family name',
        sameAs: [
          'https://orcid.org/some-orcid',
          { roleName: 'Some website name', sameAs: 'some-url' },
        ],
      },
      [
        { name: 'Some article title', sameAs: 'https://orcid.org/some-orcid/work/1337' },
        { name: 'Some other article title', sameAs: 'https://orcid.org/some-orcid/work/42' },
      ],
    ]);
  });

  it('should still lists employment affliations even if there are no education affliations', () => {
    const mockMinimalResponse: GetOrcidResponse = {
      ...mockResponse,
      'activities-summary': {
        ...mockResponse['activities-summary'],
        employments: { 'employment-summary': [
          {
            'end-date': null,
            'organization': { name: 'Some employment organisation' },
            'role-title': 'Some employment role',
            'start-date': { year:  { value: '1337' }, month: { value: '04' }, day: { value: '02' } },
          },
        ] },
      },
    };

    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce({ json: () => (mockMinimalResponse) });

    return expect(getProfile('some-orcid')).resolves.toEqual([
      {
        affliation: [ {
          affliation: { name: 'Some employment organisation' },
          endDate: undefined,
          roleName: 'Some employment role',
          startDate: '1337-04-02T00:00:00.000Z',
        } ],
        name: expect.anything(),
        sameAs: [ 'https://orcid.org/some-orcid' ],
      },
      [],
    ]);
  });
});

describe('getWork', () => {
  const mockResponse: GetOrcidWorkResponse = {
    'external-ids': null,
    'journal-title': null,
    'publication-date': null,
    'short-description': null,
    'title': {
      subtitle: null,
      title: { value: 'Arbitrary title' },
    },
    'url': null,
  };

  it('should return a ScholarlyArticle for a minimal API response', () => {
    const mockMinimalResponse: GetOrcidWorkResponse = {
      ...mockResponse,
      title: {
        ...mockResponse.title,
        title: { value: 'Some title' },
      },
    };

    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce({ json: () => (mockMinimalResponse) });

    return expect(getWork('some-orcid', 'arbitrary-putcode')).resolves.toEqual({
      author: [ { sameAs: 'https://orcid.org/some-orcid' } ],
      name: 'Some title',
    });
  });

  it('should return a ScholarlyArticle for a maximally detailed API response', () => {
    const mockMaximalResponse: GetOrcidWorkResponse = {
      'external-ids': { 'external-id': [] },
      'journal-title': { value: 'Arbitrary journal' },
      'publication-date': {
        day: { value: '02' },
        month: { value: '07' },
        year: { value: '1337' },
      },
      'short-description': 'Some abstract',
      'title': {
        subtitle: { value: 'Arbitrary subtitle' },
        title: { value: 'Some title' },
      },
      'url': { value: 'some-url' },
    };

    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce({ json: () => (mockMaximalResponse) });

    return expect(getWork('some-orcid', 'arbitrary-putcode')).resolves.toEqual({
      author: [ { sameAs: 'https://orcid.org/some-orcid' } ],
      datePublished: '1337-07-02T00:00:00.000Z',
      description: 'Some abstract',
      name: 'Some title',
      sameAs: 'some-url',
    });
  });

  it('should construct an DOAI URL using the DOI if one is provided', () => {
    const mockDoiResponse: GetOrcidWorkResponse = {
      'external-ids': { 'external-id': [
        { 'external-id-type': 'doi', 'external-id-value': 'some-doi' },
      ] },
      'journal-title': { value: 'Arbitrary journal' },
      'publication-date': {
        day: { value: '02' },
        month: { value: '07' },
        year: { value: '1337' },
      },
      'short-description': 'Some abstract',
      'title': {
        subtitle: { value: 'Arbitrary subtitle' },
        title: { value: 'Some title' },
      },
      'url': { value: 'some-url' },
    };

    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce({ json: () => (mockDoiResponse) });

    return expect(getWork('some-orcid', 'arbitrary-putcode')).resolves.toEqual(
      expect.objectContaining({
      sameAs: 'http://doai.io/some-doi',
    }));
  });

  it('should look up the full texts of works with a DOI', () => {
    const mockDoiResponse: GetOrcidWorkResponse = {
      'external-ids': { 'external-id': [
        { 'external-id-type': 'doi', 'external-id-value': 'some-doi' },
      ] },
      'journal-title': { value: 'Arbitrary journal' },
      'publication-date': {
        day: { value: '02' },
        month: { value: '07' },
        year: { value: '1337' },
      },
      'short-description': 'Some abstract',
      'title': {
        subtitle: { value: 'Arbitrary subtitle' },
        title: { value: 'Some title' },
      },
      'url': { value: 'some-url' },
    };

    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce({ json: () => (mockDoiResponse) });

    const mockedUnpaywallService = require.requireMock('../../src/services/unpaywall');
    mockedUnpaywallService.getOpenAccessLocation.mockReturnValueOnce(Promise.resolve('some-pdf-url'));

    return expect(getWork('some-orcid', 'arbitrary-putcode')).resolves.toEqual(
      expect.objectContaining({
        associatedMedia: expect.arrayContaining([ {
          contentUrl: 'some-pdf-url',
          name: 'some-doi.pdf',
        } ]),
      }),
    );
  });

  it('should look not add a link to full text versions of works with a DOI if none is available', () => {
    const mockDoiResponse: GetOrcidWorkResponse = {
      'external-ids': { 'external-id': [
        { 'external-id-type': 'doi', 'external-id-value': 'some-doi' },
      ] },
      'journal-title': { value: 'Arbitrary journal' },
      'publication-date': {
        day: { value: '02' },
        month: { value: '07' },
        year: { value: '1337' },
      },
      'short-description': 'Some abstract',
      'title': {
        subtitle: { value: 'Arbitrary subtitle' },
        title: { value: 'Some title' },
      },
      'url': { value: 'some-url' },
    };

    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce({ json: () => (mockDoiResponse) });

    const mockedUnpaywallService = require.requireMock('../../src/services/unpaywall');
    mockedUnpaywallService.getOpenAccessLocation.mockReturnValueOnce(Promise.resolve(null));

    return expect(getWork('some-orcid', 'arbitrary-putcode')).resolves.not.toEqual(
      expect.objectContaining({
        associatedMedia: expect.anything(),
      }),
    );
  });

  it('should look not add a link to full text versions of works with a DOI if there was an error fetching it', () => {
    const mockDoiResponse: GetOrcidWorkResponse = {
      'external-ids': { 'external-id': [
        { 'external-id-type': 'doi', 'external-id-value': 'some-doi' },
      ] },
      'journal-title': { value: 'Arbitrary journal' },
      'publication-date': {
        day: { value: '02' },
        month: { value: '07' },
        year: { value: '1337' },
      },
      'short-description': 'Some abstract',
      'title': {
        subtitle: { value: 'Arbitrary subtitle' },
        title: { value: 'Some title' },
      },
      'url': { value: 'some-url' },
    };

    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce({ json: () => (mockDoiResponse) });

    const mockedUnpaywallService = require.requireMock('../../src/services/unpaywall');
    mockedUnpaywallService.getOpenAccessLocation.mockReturnValueOnce(Promise.reject('arbitrary error'));

    return expect(getWork('some-orcid', 'arbitrary-putcode')).resolves.not.toEqual(
      expect.objectContaining({
        associatedMedia: expect.anything(),
      }),
    );
  });

  it('should link to the first given (potentially paywalled) URL if no DOI nor PDF is available', () => {
    const mockExternalIdResponse: GetOrcidWorkResponse = {
      'external-ids': { 'external-id': [
        { 'external-id-type': 'cba', 'external-id-url': { value: 'https://some-external-url' } },
        { 'external-id-type': 'pmc', 'external-id-url': { value: 'https://arbitrary-external-url' } },
      ] },
      'journal-title': { value: 'Arbitrary journal' },
      'publication-date': {
        day: { value: '02' },
        month: { value: '07' },
        year: { value: '1337' },
      },
      'short-description': 'Some abstract',
      'title': {
        subtitle: { value: 'Arbitrary subtitle' },
        title: { value: 'Some title' },
      },
      'url': { value: 'some-url' },
    };

    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce({ json: () => (mockExternalIdResponse) });

    return expect(getWork('some-orcid', 'arbitrary-putcode')).resolves.toEqual({
      author: [ { sameAs: 'https://orcid.org/some-orcid' } ],
      datePublished: '1337-07-02T00:00:00.000Z',
      description: 'Some abstract',
      name: 'Some title',
      sameAs: 'https://some-external-url',
    });
  });

  it('should link to the PDF of arXiv articles', () => {
    const mockArxivIdResponse: GetOrcidWorkResponse = {
      'external-ids': { 'external-id': [
        {
          'external-id-type': 'arxiv',
          'external-id-url': { value: 'https://arbitrary-external-url' },
          'external-id-value': 'some-arxiv-id',
        },
      ] },
      'journal-title': { value: 'Arbitrary journal' },
      'publication-date': {
        day: { value: '02' },
        month: { value: '07' },
        year: { value: '1337' },
      },
      'short-description': 'Some abstract',
      'title': {
        subtitle: { value: 'Arbitrary subtitle' },
        title: { value: 'Some title' },
      },
      'url': { value: 'some-url' },
    };

    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce({ json: () => (mockArxivIdResponse) });

    return expect(getWork('some-orcid', 'arbitrary-putcode')).resolves.toEqual({
      associatedMedia: [ {
        contentUrl: 'https://arxiv.org/pdf/some-arxiv-id',
        license: 'https://arxiv.org/licenses/nonexclusive-distrib/1.0/license.html',
        name: `arxiv-some-arxiv-id.pdf`,
      } ],
      author: [ { sameAs: 'https://orcid.org/some-orcid' } ],
      datePublished: '1337-07-02T00:00:00.000Z',
      description: 'Some abstract',
      name: 'Some title',
      sameAs: 'https://arbitrary-external-url',
    });
  });
});

describe('searchProfiles', () => {
  const mockResponse: GetOrcidProfileSearchResponse = [
    {
      'activities-summary': {
        educations: { 'education-summary': [] },
        employments: { 'employment-summary': [] },
        works: { group: [] },
      },
      'person': {
        'biography': null,
        'keywords': { keyword: [] },
        'name': {
          'family-name': null,
          'given-names': { value: 'Arbitrary name' },
        },
        'researcher-urls': { 'researcher-url': [] },
      },
    },
  ];

  it('should return the given names and ORCIDs', () => {
    const mockCompleteResponse: GetOrcidProfileSearchResponse = [
      {
        ...mockResponse[0],
        person: {
          ...mockResponse[0].person,
          name: {
            'family-name': null,
            'given-names': { value: 'Some firstname' },
            'path': 'some-orcid',
          },
        },
      },
      {
        ...mockResponse[0],
        person: {
          ...mockResponse[0].person,
          name: {
            'family-name': { value: 'Some lastname' },
            'given-names': { value: 'Some other firstname' },
            'path': 'some-other-orcid',
          },
        },
      },
    ];

    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce({ json: () => (mockCompleteResponse) });

    return expect(searchProfiles('some-orcid')).resolves.toEqual([
      {
        name: 'Some firstname',
        sameAs: 'https://orcid.org/some-orcid',
      },
      {
        name: 'Some other firstname Some lastname',
        sameAs: 'https://orcid.org/some-other-orcid',
      },
    ]);
  });
});
