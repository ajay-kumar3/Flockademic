// We merely need the type definitions here:
// tslint:disable-next-line:no-implicit-dependencies
import { APIGatewayEvent, Callback } from 'aws-lambda';
import * as pathToRegexp from 'path-to-regexp';

import { getCorsHeaders } from './getCorsHeaders';

interface Faas {
  default: (middleware: Middleware<undefined>) => void;
  get: (route: string, middleware: Middleware<undefined>) => void;
  post: <RequestBody>(route: string, middleware: Middleware<RequestBody>) => void;
  put: <RequestBody>(route: string, middleware: Middleware<RequestBody>) => void;
}

// The `any` here is disappointing, but at the moment I'm not aware of a better way
// to indicate that middleware can require additional properties on `context`.
export type Middleware<RequestBody> = (context: Request<RequestBody> & any) => Promise<Response>;

export interface Request<Body> {
  headers: { [name: string]: string };
  method: 'GET' | 'POST' | 'PUT';
  params: string[] | null;
  path: string;
  query: { [name: string]: string } | null;

  body: Partial<Body>;
}

export type Response = object;

export function initialise(event: APIGatewayEvent, callback?: Callback): Faas {
  let hasMatch = false;
  let defaultSet = false;
  const routesPerMethod: { [method: string]: string[] } = {};

  const use = <RequestBody>(route: string, middleware: Middleware<RequestBody>) => {
    if (defaultSet) {
      // tslint:disable-next-line:no-console
      console.log('Routes added after setting the default route.');

      return;
    }

    const tail = '/' + ((!event.pathParameters || !event.pathParameters.tail) ? '' : event.pathParameters.tail);

    const re = pathToRegexp(route);

    if (hasMatch || !re.test(tail)) {
      return;
    }
    hasMatch = true;

    if (!callback) {
      // tslint:disable-next-line:no-console
      console.log('No response callback defined by API Gateway.');

      return;
    }

    const params = re.exec(tail);

    const body: Partial<RequestBody> = (!event.body) ? undefined : JSON.parse(event.body);
    const request: Request<RequestBody> = {
      body,
      headers: event.headers,
      method: (event.httpMethod as 'GET' | 'POST' | 'PUT'),
      params,
      path: tail,
      query: event.queryStringParameters,
    };

    middleware(request)
    .then((response: object) => {
      const responseHeaders = getCorsHeaders(event.headers);
      responseHeaders['Content-Type'] = 'application/json';

      callback(undefined, {
        body: JSON.stringify(response),
        headers: responseHeaders,
        statusCode: 200,
      });
    })
    .catch((err: Error) => {
      const responseHeaders = getCorsHeaders(event.headers);
      responseHeaders['Content-Type'] = 'text/plain';

      callback(undefined, {
        body: err.message,
        headers: responseHeaders,
        statusCode: 400,
      });
    });
  };

  const useForMethod = <RequestBody>(
    method: 'GET' | 'PUT' | 'POST',
    route: string, middleware: Middleware<RequestBody>,
  ) => {
    routesPerMethod[method] = routesPerMethod[method] || [];
    routesPerMethod[method].push(route);

    if (event.httpMethod === method) {
      use(route, middleware);
    }
  };

  const setDefault = (middleware: Middleware<undefined>) => {
    if (event.httpMethod === 'OPTIONS') {
      const responseHeaders = getCorsHeaders(event.headers);

      // Note: we currently allow all methods for which we have a route defined.
      // We might want to narrow it down to methods for which this route matches?
      responseHeaders['Access-Control-Allow-Methods'] = Object.keys(routesPerMethod).join(', ');
      responseHeaders['Access-Control-Allow-Headers'] = 'Authorization';

      if (!callback) {
        // tslint:disable-next-line:no-console
        console.log('No response callback defined by API Gateway.');

        return;
      }

      callback(undefined, {
        headers: responseHeaders,
        statusCode: 200,
      });
    } else {
      use('/(.*)', middleware);
    }

    defaultSet = true;
  };

  return {
    default: setDefault,
    get: (route: string, middleware: Middleware<undefined>) => useForMethod('GET', route, middleware),
    post: <RequestBody>(route: string, middleware: Middleware<RequestBody>) => useForMethod('POST', route, middleware),
    put: <RequestBody>(route: string, middleware: Middleware<RequestBody>) => useForMethod('PUT', route, middleware),
  };
}
